const data = await Deno.readTextFile("input.txt")

//             [B]         [L]     [S]
//     [Q] [J] [C]         [W]     [F]
//     [F] [T] [B] [D]     [P]     [P]
//     [S] [J] [Z] [T]     [B] [C] [H]
//     [L] [H] [H] [Z] [G] [Z] [G] [R]
// [R] [H] [D] [R] [F] [C] [V] [Q] [T]
// [C] [J] [M] [G] [P] [H] [N] [J] [D]
// [H] [B] [R] [S] [R] [T] [S] [R] [L]
//  0   1   2   3   4   5   6   7   8 

const stacks = [
    ['H','C','R'],
    ['B','J','H','L','S','F'],
    ['R','M','D','H','J','T','Q'],
    ['S','G','R','H','Z','B','J'],
    ['R','P','F','Z','T','D','C','B'],
    ['T','H','C','G'],
    ['S','N','V','Z','B','P','W','L'],
    ['R','J','Q','G','C'],
    ['L','D','T','R','H','P','F','S']
]

const stacks2 = [
    ['H','C','R'],
    ['B','J','H','L','S','F'],
    ['R','M','D','H','J','T','Q'],
    ['S','G','R','H','Z','B','J'],
    ['R','P','F','Z','T','D','C','B'],
    ['T','H','C','G'],
    ['S','N','V','Z','B','P','W','L'],
    ['R','J','Q','G','C'],
    ['L','D','T','R','H','P','F','S']
]

class Move {
    idx: number
    amount: number
    source: number
    target: number
    original: string
    
    constructor(idx: number, value: string) {
        this.idx = idx
        this.original = value

        const numbers: number[] = value.match(/\d+/g)?.map(char => parseInt(char)) as number[]
        this.amount = numbers[0]
        this.source = numbers[1] - 1
        this.target = numbers[2] - 1
    }

    toString(): string {
        return `Move #${this.idx}: ${this.amount} create(s) from ${this.source} to ${this.target} (Created from "${this.original}")`
    }
}

// Extract moves
const moves: Move[] = []
const lines = data.split("\r\n")

for(let i=0; i<lines.length; i++) {
    const realIdx = i+1
    const move: Move = new Move(realIdx, lines[i])
    moves.push(move)
}

// Move elements...
for(const move of moves) {
    const temp = stacks[move.source].splice(move.amount*-1)
    temp.reverse()
    stacks[move.target].push(...temp)
}

for(const move of moves) {
    const temp = stacks2[move.source].splice(move.amount*-1)
    stacks2[move.target].push(...temp)
}

console.log("Part 1", stacks.map(stack => stack[stack.length-1]).join(""))
console.log("Part 2", stacks2.map(stack => stack[stack.length-1]).join(""))