const data = await Deno.readTextFile("input.txt")

const pairs = data.split("\r\n")

const ranges = pairs.map(str => str.split(',')
    .map(range => range.split('-').map(char => parseInt(char)))
)

const includingCondition = (pairs: number[][]) => 
    (pairs[0][0] >= pairs[1][0] && pairs[0][1] <= pairs[1][1]) ||
    (pairs[0][0] <= pairs[1][0] && pairs[0][1] >= pairs[1][1])

const overlapingCondition = (pairs: number[][]) =>
(pairs[0][0] >= pairs[1][0] && pairs[0][0] <= pairs[1][1]) ||
(pairs[0][1] >= pairs[1][0] && pairs[0][1] <= pairs[1][1]) ||
(pairs[1][0] >= pairs[0][0] && pairs[1][0] <= pairs[0][1]) ||
(pairs[1][1] >= pairs[0][0] && pairs[1][1] <= pairs[0][1])

console.log("Part 1", ranges.filter(includingCondition).length)
console.log("Part 2", ranges.filter(overlapingCondition).length)