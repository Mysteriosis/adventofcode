const data = await Deno.readTextFile("input.txt")
const listOfElves = data.split("\r\n\r\n")
    .map(elf => elf.split("\r\n"))
    .map(elf => elf.reduce((sub, cal) => sub + parseInt(cal), 0))
    .sort((a,b) => a-b).reverse()

console.log("Part 1", listOfElves[0])
console.log("Part 2", listOfElves.slice(0,3).reduce((sub, cal) => sub + cal, 0))