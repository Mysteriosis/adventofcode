const data = await Deno.readTextFile("input.txt")

// A for Rock => 1
// B for Paper => 2
// C for Scissors => 3
// Win => 6
// Draw => 3
// Loss => 0

const part1 = {
    AX: 1 + 3,
    AY: 2 + 6,
    AZ: 3 + 0,
    BX: 1 + 0,
    BY: 2 + 3,
    BZ: 3 + 6,
    CX: 1 + 6,
    CY: 2 + 0,
    CZ: 3 + 3,
}

const part2 = {
    AX: 3 + 0,
    AY: 1 + 3,
    AZ: 2 + 6,
    BX: 1 + 0,
    BY: 2 + 3,
    BZ: 3 + 6,
    CX: 2 + 0,
    CY: 3 + 3,
    CZ: 1 + 6,
}

const listOfPlay = data.split("\r\n").map(strat => strat.replace(" ", ""))

console.log("Part1", listOfPlay.reduce((acc, strat) => acc + part1[strat as keyof typeof part1], 0))
console.log("Part2", listOfPlay.reduce((acc, strat) => acc + part2[strat as keyof typeof part2], 0))