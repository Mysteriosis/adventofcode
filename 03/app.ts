const data = await Deno.readTextFile("input.txt")

const items = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const groupSize = 3

const rucksacks = data.split("\r\n")

const rucksacksSeparated = rucksacks.map(content => [content.slice(0, content.length/2), content.slice(content.length/2)])
const rucksackCommonItems = rucksacksSeparated.map(rs => rs[0].split('').filter(item => rs[1].includes(item))[0])

const groupOfThreeRucksacks = Array.from(new Array(rucksacks.length / groupSize), (_, i) => rucksacks.slice(i * groupSize, i * groupSize + groupSize));
const groupCommonItems = groupOfThreeRucksacks.map(rs => rs[0].split('').filter(item => rs[1].includes(item) && rs[2].includes(item))[0])

console.log("Part 1", rucksackCommonItems.reduce((acc, item) => acc + items.indexOf(item as string) + 1, 0))
console.log("Part 2", groupCommonItems.reduce((acc, item) => acc + items.indexOf(item as string) + 1, 0))